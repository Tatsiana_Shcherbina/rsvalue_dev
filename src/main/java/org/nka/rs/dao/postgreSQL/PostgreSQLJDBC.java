package org.nka.rs.dao.postgreSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by zgurskaya on 17.06.2016.
 */
public class PostgreSQLJDBC {

    public static final String URL = "jdbc:postgresql://172.31.14.75/test_convert";
    public static final String URL_TEST = "jdbc:postgresql://172.31.14.46/dev";
    public static final String URL_LIVE = "jdbc:postgresql://172.31.14.45/pbgisdb";

    public static final String LOGIN = "rvalue";

    public static final String PASSWORD = "2%#hEM5du1";
    public static final String PASSWORD_TEST = "EGrpe(0)3C";
    public static final String PASSWORD_LIVE = "n?Zp*FzM}R:ud=x^LhF6";



    public Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(URL_TEST, LOGIN, PASSWORD_TEST);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
