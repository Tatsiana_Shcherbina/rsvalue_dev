package org.nka.rs.dao;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */
public interface ILoadDocTypeDao<LoadDocumentTypeDic> extends IBaseDao {

    List<LoadDocumentTypeDic> getNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType);
    List<LoadDocumentTypeDic> getNotNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType);

}
