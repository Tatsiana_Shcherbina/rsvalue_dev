package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.Group;

import java.util.List;

/**
 * Created by tihonovichp on 08.11.2016.
 */
public interface IGroupDao extends IBaseDao<Group>{

    List<String> getGroupsNames();

    Integer getIdByName(String name);
}
