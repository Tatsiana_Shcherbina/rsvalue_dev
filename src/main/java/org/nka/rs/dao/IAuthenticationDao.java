package org.nka.rs.dao;

/**
 * Created by zgurskaya on 04.10.2016.
 */
public interface IAuthenticationDao extends IBaseDao{

    //получить userId по логину
    Long getUserId(String userName);
    //получить ФИО по логину
    String getPersonData(String userName);

    void logout(String username, String token);

}
