package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.UserEntity;

import java.util.List;

/**
 * Created by tihonovichp on 03.11.2016.
 */
public interface IUserEntityDao extends IBaseDao<UserEntity>{

    UserEntity getUserByName(String username);

    Long getIdByName(String username);

    List<String> getLogins();

    void logout(String username, String token);

    String createAndSaveToken(Long id, String username);

    String getToken(Long id);
}
