package org.nka.rs.dao;

import org.nka.rs.entity.dictionary.TORDic;

import java.util.List;

/**
 * Created by tihonovichp on 08.11.2016.
 */
public interface ITorDao extends IBaseDao<TORDic>{

    List<String> getCodeNames();

    Integer getCodeByName(String name);

}
