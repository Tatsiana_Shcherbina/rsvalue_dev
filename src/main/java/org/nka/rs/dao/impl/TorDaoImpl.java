package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.nka.rs.dao.ITorDao;
import org.nka.rs.entity.dictionary.TORDic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tihonovichp on 08.11.2016.
 */

@Repository
public class TorDaoImpl extends BaseDaoSessionImpl<TORDic> implements ITorDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<String> getCodeNames() {
        return sessionFactory.getCurrentSession().createQuery("Select td.codeName from TORDic td").list();
    }

    @Override
    public Integer getCodeByName(String name) {
        return (Integer) sessionFactory.getCurrentSession().createQuery("Select td.analyticCode from TORDic td where codeName=:codeName").setString("codeName", name).uniqueResult();
    }
}
