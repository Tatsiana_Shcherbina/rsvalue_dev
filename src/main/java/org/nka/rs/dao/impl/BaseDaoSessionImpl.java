package org.nka.rs.dao.impl;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.nka.rs.dao.IBaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tihonovichp on 11.11.2016.
 */

@Repository
public class BaseDaoSessionImpl<E> implements IBaseDao<E> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<E> getAllElements(Class<E> clazz) {
        return sessionFactory.getCurrentSession().createCriteria(clazz).list();
    }

    @Override
    public E getElementByID(Class<E> clazz, Serializable id) {
        return (E) sessionFactory.getCurrentSession().get(clazz, id);
    }

    @Override
    public Serializable addElement(E e) {
        return sessionFactory.getCurrentSession().save(e);
    }

    @Override
    public void updateElement(E el) {
        sessionFactory.getCurrentSession().update(el);
    }

    @Override
    public void updateElementById(Class<E> clazz, Serializable id) {
        E el = getElementByID(clazz, id);
        updateElement(el);
    }

    @Override
    public void refreshElement(E el) {
        sessionFactory.getCurrentSession().refresh(el);
    }

    @Override
    public void refreshElementById(Class<E> clazz, Serializable id) {
        E el = getElementByID(clazz, id);
        refreshElement(el);
    }

    @Override
    public void deleteElement(E el) {
        sessionFactory.getCurrentSession().delete(el);
    }

    @Override
    public void deleteElementById(Class<E> clazz, Serializable id) {
        E el = getElementByID(clazz, id);
        deleteElement(el);
    }

    @Override
    public List<E> getCriterion(DetachedCriteria crio) {
        return crio.getExecutableCriteria(sessionFactory.getCurrentSession()).list();
    }

    @Override
    public E getUniqueResult(DetachedCriteria crio) {
        return (E) crio.getExecutableCriteria(sessionFactory.getCurrentSession()).uniqueResult();
    }
}
