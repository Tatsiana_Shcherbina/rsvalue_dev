package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.Query;
import org.hibernate.Session;
import org.nka.rs.dao.IAddressDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.pojos.load.ATE;
import org.nka.rs.util.connection.UtilConnection;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 03.06.2016.
 */

@Repository
public class AddressDaoImpl extends BaseDaoSessionImpl implements IAddressDao<ATE>, IConstant {


    @Override
    public List<ATE> findAteForCityCostType(Long parentateId) {
        Session session = null;
        List<ATE> result = null;
        try {
            session = sessionFactory.getCurrentSession();
            StringBuffer queryStr = new StringBuffer("From ATE ate where ");
            if (parentateId != null) {
                queryStr.append(" ate.parentAteId = :parentAte ");
            } else {
                queryStr.append(" ate.parentAteId is null ");
            }
            queryStr.append(" AND ate.parentCategory in (:area, :region, :city) AND ate.actual = :relevant order by ate.ateName");

            System.out.print(queryStr);
            Query q = session.createQuery(queryStr.toString());

            if (parentateId != null) {
                q.setLong("parentAte", parentateId);
            }
            q.setInteger("area", AREA);
            q.setInteger("region", REGION);
            q.setInteger("city", CITY);
            q.setBoolean("relevant", false);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<ATE> findAteForOtherCostType(Long parentateId) {
        Session session = null;
        List<ATE> result = null;
        try {
            session = sessionFactory.getCurrentSession();
            StringBuffer queryStr = new StringBuffer("From ATE ate where ");
            if (parentateId != null) {
                queryStr.append(" ate.parentAteId = :parentAte ");
            } else {
                queryStr.append(" ate.parentAteId is null ");
            }
            queryStr.append(" AND ate.parentCategory in (:area, :region) AND ate.actual = :relevant order by ate.ateName");

            //System.out.print(queryStr);
            Query q = session.createQuery(queryStr.toString());
            if (parentateId != null) {
                q.setLong("parentAte", parentateId);
            }
            q.setInteger("area", AREA);
            q.setInteger("region", REGION);
            q.setBoolean("relevant", false);


            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Long> findAteIdForParcelOutNP(Long parentateId) {
        Session session = null;
        List<Long> result = null;
        try {
            session = sessionFactory.getCurrentSession();
            StringBuffer queryStr = new StringBuffer("Select ate.ateId From ATE ate where ");
            if (parentateId != null) {
                queryStr.append(" ate.parentAteId = :parentAte ");
            } else {
                queryStr.append(" ate.parentAteId is null ");
            }
            queryStr.append(" AND ate.parentCategory = :villageCouncil AND ate.actual = :relevant  order by ate.ateName");

            System.out.print(queryStr);
            Query q = session.createQuery(queryStr.toString());
            if (parentateId != null) {
                q.setLong("parentAte", parentateId);
            }
            q.setInteger("villageCouncil", VILLAGE_COUNCIL);
            q.setBoolean("relevant", false);


            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Long> findAteIdForSNP(Long parentateId) {
        Connection connection = null;
        CallableStatement call = null;
        ResultSet res = null;
        List<Long> result = new ArrayList<Long>();
        try {
            connection = UtilConnection.getPooledConnection();
            call = connection.prepareCall("{ ? = call " + SCHEMA_NAME + ".PKG_LOAD.GET_SNP(?)}");

            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, parentateId);
            call.execute();

            res = (ResultSet) call.getObject(1);
            while (res.next()) {
                result.add(res.getLong(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (res != null) {
                    res.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (call != null) {
                    call.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
