package org.nka.rs.dao.impl;

import oracle.jdbc.OracleTypes;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.jdbc.ReturningWork;
import org.nka.rs.dao.IUserAccountDao;
import org.nka.rs.entity.dictionary.TORDic;
import org.nka.rs.entity.pojos.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tihonovichp on 03.11.2016.
 */

@Repository
public class UserAccountDaoImpl extends BaseDaoSessionImpl<UserAccount> implements IUserAccountDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<UserAccount> getAllElements(Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua order by ua.id")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    @Override
    public Serializable addElement(final UserData userData) {
        final String[] fullName = userData.getFio().split(" ");
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Serializable>() {
            @Override
            public Serializable execute(Connection connection) throws SQLException {
                CallableStatement callableStatement = null;
                String sqlString = "{? = call RS_VALUE_DEV.PKG_USERS.INS_USER(?,?,?,?,?,?,?,?,?)}";
                callableStatement = connection.prepareCall(sqlString);
                callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
                callableStatement.setString(2, fullName[0]);
                callableStatement.setString(3, fullName[1]);
                callableStatement.setString(4, fullName[2]);
                callableStatement.setString(5, userData.getUsername());
                callableStatement.setString(6, userData.getPassword());
                callableStatement.setInt(7, userData.getOrganization());
                callableStatement.setInt(8, userData.getGroup());
                callableStatement.setString(9, userData.getPosition());
                callableStatement.setLong(10, 1L);
                callableStatement.executeQuery();
                callableStatement.getInt(1);
                return callableStatement.getInt(1);
            }
        });
    }

    @Override
    public Long getRowCount() {
        return (Long) sessionFactory.getCurrentSession().createCriteria(UserAccount.class).setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public UserAccount getFullAccount(Long id) {
        UserAccount userAccount = getElementByID(UserAccount.class, id);
        System.out.println(userAccount);
        return userAccount;
    }

    @Override
    public UserAccount getAccountByName(String username) {
        return (UserAccount) sessionFactory.getCurrentSession().createQuery("from UserAccount ua where ua.userEntity.username=:username")
                .setString("username", username)
                .uniqueResult();
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) " +
                        "like :login and lower(ua.regCode.codeName) like :org and lower(ua.profession) like :position and " +
                        "lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("position", "%" + position + "%")
                .setString("group", group + "%")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount) {
        return sessionFactory.getCurrentSession().createQuery
                ("Select ua.id, ua.profession, ua.regCode.codeName, ua.subject.surname, ua.subject.firstName, " +
                        "ua.subject.fatherName, ua.userEntity.username, ua.userEntity.group.groupName " +
                        "from UserAccount ua where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) " +
                        "like :login and lower(ua.regCode.codeName) like :org and lower(ua.userEntity.group.groupName) " +
                        "like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("group", group + "%")
                .setFirstResult(startRecord)
                .setMaxResults(recordCount)
                .list();
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String position, String group) {
        return (Long) sessionFactory.getCurrentSession().createQuery("Select count(*) from UserAccount ua " +
                "where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) like :login and " +
                "lower(ua.regCode.codeName) like :org and lower(ua.profession) like :position and " +
                "lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("position", "%" + position + "%")
                .setString("group", group + "%")
                .uniqueResult();
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String group) {
        return (Long) sessionFactory.getCurrentSession().createQuery("Select count(*) from UserAccount ua " +
                "where lower(ua.subject.surname) like :surname and lower(ua.userEntity.username) like :login and " +
                "lower(ua.regCode.codeName) like :org and lower(ua.userEntity.group.groupName) like :group order by ua.id")
                .setString("surname", "%" + surname + "%")
                .setString("login", "%" + login + "%")
                .setString("org", "%" + org + "%")
                .setString("group", group + "%")
                .uniqueResult();
    }

    @Override
    public void updateElements(UserData userData, boolean check) {
        UserAccount userAccount = (UserAccount) sessionFactory.getCurrentSession().get(UserAccount.class, userData.getId());
        String[] fio = userData.getFio().split(" ");
        if (check) {
            userAccount.getUserEntity().setUsername(userData.getUsername());
            userAccount.getUserEntity().setPassword(userData.getPassword());
        }
        userAccount.setProfession(userData.getPosition());
        userAccount.getSubject().setSurname(fio[0]);
        userAccount.getSubject().setFirstName(fio[1]);
        userAccount.getSubject().setFatherName(fio[2]);
        sessionFactory.getCurrentSession().update(userAccount);
    }

    @Override
    public Serializable updateElement(final UserData userData, final boolean check) {
        final String[] fullName = userData.getFio().split(" ");
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Serializable>() {
            @Override
            public Serializable execute(Connection connection) throws SQLException {
                CallableStatement callableStatement = null;
                String sqlString = "";
                if (check) {
                    sqlString = "{? = call RS_VALUE_DEV.PKG_USERS.UPD_USER2(?,?,?,?,?,?,?,?)}";
                    callableStatement = connection.prepareCall(sqlString);
                    callableStatement.setString(8, userData.getUsername());
                    callableStatement.setString(9, userData.getPassword());
                } else {
                    sqlString = "{? = call RS_VALUE_DEV.PKG_USERS.UPD_USER1(?,?,?,?,?,?)}";
                    callableStatement = connection.prepareCall(sqlString);
                }
                callableStatement.registerOutParameter(1, OracleTypes.NUMBER);
                callableStatement.setLong(2, userData.getId());
                callableStatement.setString(3, userData.getPosition());
                callableStatement.setString(4, fullName[0]);
                callableStatement.setString(5, fullName[1]);
                callableStatement.setString(6, fullName[2]);
                callableStatement.setLong(7, 1L);
                callableStatement.executeQuery();
                callableStatement.getInt(1);
                return callableStatement.getInt(1);
            }
        });
    }

    @Override
    public List<FilterResponse> getFilterFunction(final String surname, final String login, final Integer org, final Integer group, final Integer page, final Integer recordCount) {
        return sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<FilterResponse>>() {
            @Override
            public List<FilterResponse> execute(Connection connection) throws SQLException {
                String rowCount = "";
                int colCount = 2;
                int surnameCol = 0;
                int loginCol = 0;
                int orgCol = 0;
                int groupCol = 0;
                List<FilterResponse> accounts = new ArrayList<FilterResponse>();
                CallableStatement callableStatement = null;
                String sqlEnd = "v_numpage => ?, v_qtypage => ?)}";
                StringBuilder sqlString = new StringBuilder("{? = call RS_VALUE_DEV.PKG_USERS.GET_LIST_USER_FOREDIT(");
                if (!surname.equals("")) {
                    sqlString.append("v_surname => ?, ");
                    surnameCol = colCount++;
                }
                if (!login.equals("")) {
                    sqlString.append("v_login => ?, ");
                    loginCol = colCount++;
                }
                if (!(org == null)) {
                    sqlString.append("v_org => ?, ");
                    orgCol = colCount++;
                }
                if (!(group == null)) {
                    sqlString.append("v_group => ?, ");
                    groupCol = colCount++;
                }
                sqlString.append(sqlEnd);
                callableStatement = connection.prepareCall(sqlString.toString());
                callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
                if (!surname.equals("")) {
                    callableStatement.setString(surnameCol, surname);
                }
                if (!login.equals("")) {
                    callableStatement.setString(loginCol, login);
                }
                if (!(org == null)) {
                    callableStatement.setInt(orgCol, org);
                }
                if (!(group == null)) {
                    callableStatement.setInt(groupCol, group);
                }
                callableStatement.setInt(colCount++, page);
                callableStatement.setInt(colCount, recordCount);
                callableStatement.executeQuery();
                ResultSet rs = (ResultSet) callableStatement.getObject(1);
                while (rs.next()) {
                    rowCount = rs.getString(1);
                    accounts.add(initFilterResponse(rs));
                }
                return accounts;
            }
        });
    }


    private FilterResponse initFilterResponse(ResultSet rs) throws SQLException {
        FilterResponse filterResponse = new FilterResponse();
        filterResponse.setId(rs.getLong(2));
        filterResponse.setProfession(rs.getString(3));
        filterResponse.setSurname(rs.getString(4));
        filterResponse.setFirstName(rs.getString(5));
        filterResponse.setFatherName(rs.getString(6));
        filterResponse.setUserName(rs.getString(7));
        filterResponse.setGroupName(rs.getString(9));
        filterResponse.setOrgName(rs.getString(11));
        return filterResponse;
    }

    private UserAccount initUserAccount(ResultSet rs) throws  SQLException {
        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(rs.getLong(2));
        userAccount.setProfession(rs.getString(3));
        Subject subject = new Subject();
        subject.setSurname(rs.getString(4));
        subject.setFirstName(rs.getString(5));
        subject.setFatherName(rs.getString(6));
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(rs.getString(7));
        Group group = new Group();
        group.setGroupId(rs.getInt(8));
        group.setGroupName(rs.getString(9));
        userEntity.setGroup(group);
        TORDic torDic = new TORDic();
        torDic.setAnalyticCode(rs.getInt(10));
        torDic.setCodeName(rs.getString(11));
        userAccount.setRegCode(torDic);
        userAccount.setSubject(subject);
        userAccount.setUserEntity(userEntity);
        return userAccount;
    }


}
