package org.nka.rs.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.nka.rs.dao.IAuthenticationDao;
import org.nka.rs.entity.pojos.security.UserInfo;
import org.springframework.stereotype.Repository;

/**
 * Created by zgurskaya on 04.10.2016.
 */

@Repository
public class AuthenticationDaoImpl extends BaseDaoSessionImpl implements IAuthenticationDao {


    @Override
    public Long getUserId(String userName) {
        Session session = null;
        Long userId = null;
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("Select user.userId From UserAccount user where user.username LIKE :username");
            q.setString("username", userName);

            userId = (Long) q.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userId;
    }

    @Override
    public String getPersonData(String userName) {
        Session session = null;
        String userData = null;
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("Select user.subject.surname || ' ' || user.subject.firstName || ' ' || user.subject.fatherName From UserAccount user where user.username LIKE :username");
            q.setString("username", userName);

            userData = (String) q.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userData;
    }

    @Override
    public void logout(String username, String token) {
        Session session = null;
        UserInfo userInfo = null;
        try {
            session = sessionFactory.getCurrentSession();
            Query q = session.createQuery("From UserInfo user where user.userId LIKE :username");
            q.setString("username", username);
            userInfo = (UserInfo) q.uniqueResult();
            if (userInfo != null) {
                userInfo.setToken(null);
            }
            session.update(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
