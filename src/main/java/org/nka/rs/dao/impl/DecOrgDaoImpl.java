package org.nka.rs.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.nka.rs.dao.IDecOrgDao;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.DecisionOrganizationDic;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by zgurskaya on 29.09.2016.
 */
@Repository
public class DecOrgDaoImpl extends BaseDaoSessionImpl implements IDecOrgDao, IConstant {

    @Override
    public List<DecisionOrganizationDic> getActualOrganization() {
        Session session = null;
        List<DecisionOrganizationDic> result = null;
        try {
            session = sessionFactory.getCurrentSession();

            Query q = session.createQuery("From DecisionOrganizationDic decOrg where decOrg.status = :relevant");

            q.setBoolean("relevant", RELEVANT);

            result = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
