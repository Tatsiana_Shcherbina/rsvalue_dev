package org.nka.rs.dao;

import java.sql.Clob;

/**
 * Created by zgurskaya on 01.08.2016.
 */
public interface ISchedulerDao {

    void updateCurrency(Integer cur_id, Double rate);

    Clob nightClearData();

}
