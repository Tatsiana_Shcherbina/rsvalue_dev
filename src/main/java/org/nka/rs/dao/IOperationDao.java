package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.Operation;

import java.sql.Date;

/**
 * Created by tihonovichp on 11.11.2016.
 */

public interface IOperationDao extends IBaseDao<Operation>{

    Operation getElementByID(Long id);

    Long getIdByDate(Date date);
}
