package org.nka.rs.dao;

import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tihonovichp on 03.11.2016.
 */

public interface IUserAccountDao extends IBaseDao<UserAccount>{

    List<UserAccount> getAllElements(Integer startRecord, Integer recordCount);

    Serializable addElement(UserData userData);

    Long getRowCount();

    UserAccount getFullAccount(Long id);

    UserAccount getAccountByName(String username);

    List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount);

    List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount);

    Long getFilterRowCount(String surname, String login, String org, String position, String group);

    Long getFilterRowCount(String surname, String login, String org, String group);

    void updateElements(UserData userData, boolean check);

    Serializable updateElement(UserData userData, boolean check);

    List<FilterResponse> getFilterFunction(final String surname, final String login, Integer org, Integer group, Integer page, Integer recordCount);
}
