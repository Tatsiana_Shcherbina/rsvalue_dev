package org.nka.rs.entity.constant.commonConstant;

/**
 * Created by zgurskaya on 13.07.2016.
 */
public enum FuncCodeEnum {

    NON_FARM_PURPOSE(100, "Виды функционального использования земель, не относящихся к сельскохозяйственным"),
    LIVING_APARTMENT_ZONE(110, "Жилая многоквартирная зона "),
    LIVING_MANOR_ZONE(120, "Жилая усадебная зона "),
    LIVING_MANOR_ZONE_FARMSTEAD_TYPE(130, "Жилая усадебная зона деревенского типа"),
    LIVING_MANOR_ZONE_COTTAGE_TYPE(140, "Жилая усадебная зона коттеджного типа"),
    RECREATION_ZONE(150, "Рекреационная зона"),
    BUSINESS_ZONE(160, "Общественно-деловая зона"),
    PRODUCTION_ZONE(170, "Производственная зона"),
    GARDENING_ASSOTIATIONS(180, "Садоводческие товарищества"),
    AGRICULTURE_PURPOSE(200, "Виды использования сельскохозяйственных земель"),
    ARABLE_LAND(210, "Пахотные земли, залежные земли, земли под постоянными культурами"),
    IMPROVED_MEADOW_LAND(220, "Улучшенные луговые земли"),
    NATURAL_MEADOW_LAND(230, "Естественные луговые земли");

    private final String typeName;
    private final Integer typeValue;


    FuncCodeEnum(Integer typeValue, String typeName) {
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public static FuncCodeEnum searchByName(String typeName) {
        for (FuncCodeEnum param : FuncCodeEnum.values()) {
            if (typeName.equals(param.getTypeName())) {
                return param;
            }
        }
        return null;
    }
    public static FuncCodeEnum searchByValue(Integer typeValue) {
        for (FuncCodeEnum param : FuncCodeEnum.values()) {
            if (typeValue.equals(param.getTypeValue())) {
                return param;
            }
        }
        return null;
    }


    public String getTypeName() {
        return typeName;
    }

    public Integer getTypeValue() {
        return typeValue;
    }

    }
