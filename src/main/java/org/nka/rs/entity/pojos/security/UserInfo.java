package org.nka.rs.entity.pojos.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Entity
@Table(name = "USER_DETAIL")
public class UserInfo {

    @Id
    @Column(name = "USERNAME")
    private String username;

    @Column(name = "SALT")
    private String salt;

    @Column(name = "TOKEN")
    private String token;

    public UserInfo(){
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
