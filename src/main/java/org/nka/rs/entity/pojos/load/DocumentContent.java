package org.nka.rs.entity.pojos.load;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 01.06.2016.
 */

//Файл-содержание
@Entity
@Table(name = "DOCCONTENT")
public class DocumentContent {

    //ИД файл-документа
    @Id
    @Column(name = "DOC_ID", nullable = false, unique = true)
    private Long docId;

    //номер документа
    @Column(name = "DOC_NUMBER", length = 30)
    private String docNumber;

    //расширение документа
    @Column(name = "DOC_PATH", length = 500)
    private String docPath;

    public DocumentContent() {
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }
}
