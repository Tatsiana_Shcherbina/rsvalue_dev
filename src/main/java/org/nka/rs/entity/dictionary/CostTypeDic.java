package org.nka.rs.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 01.06.2016.
 */

@Entity
@Table(name = "X_CADASTR_RATE")

public class CostTypeDic {

        @Id
        @Column(name = "ID_CODE", nullable = false)
        private Integer analyticCode;

        //русское наименование
        @Column(name = "CODE_NAME")
        private String codeName;

        //краткое наименование
        @Column(name = "CODE_SHORTNAME")
        private String codeShortName;

        //актуальность кода классификатора
        @Column(name = "STATUS", nullable = false)
        private Boolean status;

        public CostTypeDic() {
        }

        public Integer getAnalyticCode() {
            return analyticCode;
        }

        public void setAnalyticCode(Integer analyticCode) {
            this.analyticCode = analyticCode;
        }

        public String getCodeName() {
            return codeName;
        }

        public void setCodeName(String codeName) {
            this.codeName = codeName;
        }

        public String getCodeShortName() {
            return codeShortName;
        }

        public void setCodeShortName(String codeShortName) {
            this.codeShortName = codeShortName;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

    }
