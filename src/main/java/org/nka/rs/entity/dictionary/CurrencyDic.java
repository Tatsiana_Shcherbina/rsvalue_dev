package org.nka.rs.entity.dictionary;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by zgurskaya on 27.07.2016.
 */

@Entity
@Table(name = "X_CURRENCY")
public class CurrencyDic {

    @Id
    @Column(name = "ID_CODE")
    private Integer analyticCode;

    //русское наименование
    @Column(name = "CODE_NAME")
    private String codeName;

    //краткое наименование
    @Column(name = "CODE_SHORTNAME")
    private String codeShortName;

    //
    @Column(name = "IS_FOREIGN")
    private Boolean isForeign;

    //
    @Column(name = "ID_NBRB")
    private Integer idNbrb;

    //актуальность кода классификатора
    @Column(name = "STATUS")
    private Boolean status;


    public CurrencyDic() {
    }

    public Integer getAnalyticCode() {
        return analyticCode;
    }

    public void setAnalyticCode(Integer analyticCode) {
        this.analyticCode = analyticCode;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeShortName() {
        return codeShortName;
    }

    public void setCodeShortName(String codeShortName) {
        this.codeShortName = codeShortName;
    }

    public Boolean getForeign() {
        return isForeign;
    }

    public void setForeign(Boolean foreign) {
        isForeign = foreign;
    }

    public Integer getIdNbrb() {
        return idNbrb;
    }

    public void setIdNbrb(Integer idNbrb) {
        this.idNbrb = idNbrb;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}



