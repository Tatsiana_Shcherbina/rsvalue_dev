package org.nka.rs.entity.dto;

/**
 * Created by zgurskaya on 13.07.2016.
 */
public class ValidZoneVal {


    private Zone[] zones;
    private Integer costTypeId;
    private Long objectnumber;

    public ValidZoneVal() {
    }

    public Zone[] getZones() {
        return zones;
    }

    public void setZones(Zone[] zones) {
        this.zones = zones;
    }

    public Integer getCostTypeId() {
        return costTypeId;
    }

    public void setCostTypeId(Integer costTypeId) {
        this.costTypeId = costTypeId;
    }

    public Long getObjectnumber() {
        return objectnumber;
    }

    public void setObjectnumber(Long objectnumber) {
        this.objectnumber = objectnumber;
    }
}
