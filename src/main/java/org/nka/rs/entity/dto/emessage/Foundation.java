package org.nka.rs.entity.dto.emessage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zgurskaya on 26.09.2016.
 */
public class Foundation {


    private static final String PART_1 = "Запись в регистре стоимости земель, земельных участков государственного земельного кадастра от ";
    private static final String PART_2 = " решение об утверждении  результатов кадастровой оценки земель";

    //номер загрузки
    private Long regNum;
    //дата загрузки
    private Date dateRegStart;
    //дата решения об оценке
    private Date decDate;
    //номер решения
    private String decNum;
    //организация, принявшая решение - в родительном падеже
    private String organizationPrintName;

    public Foundation() {
    }

    public Long getRegNum() {
        return regNum;
    }

    public void setRegNum(Long regNum) {
        this.regNum = regNum;
    }

    public Date getDateRegStart() {
        return dateRegStart;
    }

    public void setDateRegStart(Date dateRegStart) {
        this.dateRegStart = dateRegStart;
    }

    public Date getDecDate() {
        return decDate;
    }

    public void setDecDate(Date decDate) {
        this.decDate = decDate;
    }

    public String getDecNum() {
        return decNum;
    }

    public void setDecNum(String decNum) {
        this.decNum = decNum;
    }

    public String getOrganizationPrintName() {
        return organizationPrintName;
    }

    public void setOrganizationPrintName(String organizationPrintName) {
        this.organizationPrintName = organizationPrintName;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return PART_1 + formatter.format(dateRegStart) + " г. №"  + regNum + ", " + PART_2  + " " + organizationPrintName + " от " + formatter.format(decDate) + " г. №" + decNum;
    }
}
