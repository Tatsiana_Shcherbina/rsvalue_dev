package org.nka.rs.entity.dto.emessage;

import java.util.Date;

/**
 * Created by zgurskaya on 26.09.2016.
 */
public class CadastralValueData {

    //функциональное назначение
    private String funcCodeName;
    //дата оценки
    private Date dateVal;
    //номер оценочной зоны
    private String zoneNum;
    //стоимость в долларах
    private Double costD;
    //стоимость в рублях
    private Double costR;
    //значение налоговой базы
    private Double costNB;
    //основание
    private Foundation foundation;
    //основание в виде строки
    private  String foundationStr;

    public CadastralValueData() {
    }

    public String getFuncCodeName() {
        return funcCodeName;
    }

    public void setFuncCodeName(String funcCodeName) {
        this.funcCodeName = funcCodeName;
    }

    public Date getDateVal() {
        return dateVal;
    }

    public void setDateVal(Date dateVal) {
        this.dateVal = dateVal;
    }

    public String getZoneNum() {
        return zoneNum;
    }

    public void setZoneNum(String zoneNum) {
        this.zoneNum = zoneNum;
    }

    public Double getCostD() {
        return costD;
    }

    public void setCostD(Double costD) {
        this.costD = costD;
    }

    public Double getCostR() {
        return costR;
    }

    public void setCostR(Double costR) {
        this.costR = costR;
    }

    public Double getCostNB() {
        return costNB;
    }

    public void setCostNB(Double costNB) {
        this.costNB = costNB;
    }

    public Foundation getFoundation() {
        return foundation;
    }

    public void setFoundation(Foundation foundation) {
        this.foundation = foundation;
    }

    public String getFoundationStr() {
        return foundationStr;
    }

    public void setFoundationStr(String foundationStr) {
        this.foundationStr = foundationStr;
    }
}
