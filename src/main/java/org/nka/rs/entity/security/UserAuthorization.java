package org.nka.rs.entity.security;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public class UserAuthorization {
    private int code;
    private int userId;
    private String login;
    private String token;
    private String message;


    public UserAuthorization() {
    }

    public UserAuthorization(int code, int userId, String login, String token, String message) {
        this.code = code;
        this.userId = userId;
        this.login = login;
        this.token = token;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAuthorization that = (UserAuthorization) o;

        if (code != that.code) return false;
        if (userId != that.userId) return false;
        if (!login.equals(that.login)) return false;
        if (!token.equals(that.token)) return false;
        return message.equals(that.message);

    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + userId;
        result = 31 * result + login.hashCode();
        result = 31 * result + token.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserAuthorization{" +
                "code=" + code +
                ", userId=" + userId +
                ", login='" + login + '\'' +
                ", token='" + token + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
