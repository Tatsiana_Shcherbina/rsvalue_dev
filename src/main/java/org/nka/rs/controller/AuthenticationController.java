package org.nka.rs.controller;

import org.nka.rs.entity.pojos.security.UserDetail;
import org.nka.rs.entity.security.RestAuthentication;
import org.nka.rs.entity.security.UserAuthorization;
import org.nka.rs.service.ISubjectService;
import org.nka.rs.service.IUserEntityService;
import org.nka.rs.service.auth.AuthenticationService;
import org.nka.rs.util.filters.PreAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zgurskaya on 20.09.2016.
 */


@RestController
@RequestMapping(value = "/auth")

public class AuthenticationController {

    @Autowired
    @Qualifier(value = "authenticationManager")
    AuthenticationManager authenticationManager;

    @Autowired
    ISubjectService subjectService;

    @Autowired
    IUserEntityService userEntityService;

    @Autowired
    PreAuthenticationFilter preAuthenticationFilter = new PreAuthenticationFilter();

    private final static String USERNAME = "username";
    private final static String PASSWORD = "password";
    private final static String TOKEN = "token";

    private final static String PASS = "5E955C0BE3CA0DA957742A511EFC8C5003171D6334A2B7B03156F9D441AB1C6A0D809E509BB10F01931900CD6311CEA79A1053BA4FB702990D8A9992E8B77711";


    @RequestMapping(value = "/greeting", method = RequestMethod.POST)
    @ResponseBody
    public RestAuthentication authenticationUser(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        return preAuthenticationFilter.attemptAuthentication(
                servletRequest.getParameter(USERNAME), servletRequest.getParameter(PASSWORD));
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.POST)
    @ResponseBody
    public RestAuthentication getUserOld(String username, String token) {
        return preAuthenticationFilter.getUser(username, token);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public void logout(HttpServletRequest request) {
        try {
            userEntityService.logout(request.getParameter(USERNAME), request.getParameter(TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/greetingNew", method = RequestMethod.POST)
    @ResponseBody
    public UserAuthorization authenticationUserNice(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        AuthenticationService authenticationService = new AuthenticationService();
        return authenticationService.isAuth(servletRequest.getParameter(USERNAME), servletRequest.getParameter(PASSWORD));
    }

    @RequestMapping(value = "/getUserNew", method = RequestMethod.POST)
    @ResponseBody
    public UserDetail getUser(String username, String token) {
        return null;
    }
}


