package org.nka.rs.util.nativedll;

import com.sun.jna.*;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public interface NativeInterface extends Library{

    //NativeInterface INSTANCE = (NativeInterface) Native.loadLibrary("MyDll.dll", NativeInterface.class);

    NativeInterface INSTANCE = (NativeInterface) Native.loadLibrary("x64RSGenLib.dll", NativeInterface.class);

    WString SHA512CPPHash(WString str);

}
