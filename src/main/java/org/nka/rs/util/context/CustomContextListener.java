package org.nka.rs.util.context;

import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * Created by tihonovichp on 14.12.2016.
 */

@WebListener
public class CustomContextListener extends ContextLoaderListener {

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("destroy CustomContextListener");
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                System.out.println(String.format("deregistering jdbc driver: %s", driver));
            } catch (SQLException e) {
                System.out.println(String.format("Error deregistering driver %s", driver));
            }
        }
        super.contextDestroyed(event);
    }
}
