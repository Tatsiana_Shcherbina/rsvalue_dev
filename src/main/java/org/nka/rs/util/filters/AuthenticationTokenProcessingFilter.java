package org.nka.rs.util.filters;

import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by zgurskaya on 04.10.2016.
 */

//TODO Работает с http заголовоками, которые не используются в проекте
//фильтр для проверки токена при его наличии
@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    private JdbcDaoImpl userDetailsService;


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = this.getAsHttpRequest(servletRequest);
        String accessToken = this.extractAuthTokenFromRequest(httpRequest);
        //todo есть возможность сделать токен, содержащий username - не будет необходимости передавать два параметра
        String username = httpRequest.getHeader("username");
        if (username != null) {
            try {
                if (accessToken != null) {
//                    String token = userInfoService.getToken(username);
//                    if (accessToken.equals(token)) {
//                        UserDetails user = userDetailsService.loadUserByUsername(username);
//                        if (user != null) {
//                            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
//                            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) servletRequest));
//                            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//                        }
//                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private HttpServletRequest getAsHttpRequest(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }
        return (HttpServletRequest) request;
    }

    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
        /* получить токен из заголовка*/
        String authToken = httpRequest.getHeader("token");
        /* если в заголовке токена нет, получить как параметр запроса*/
        if (authToken == null) {
            authToken = httpRequest.getParameter("token");
        }
        return authToken;
    }


    public void setUserDetailsService(JdbcDaoImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public JdbcDaoImpl getUserDetailsService() {
        return userDetailsService;
    }
}
