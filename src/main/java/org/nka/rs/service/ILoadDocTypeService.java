package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */
public interface ILoadDocTypeService<LoadDocumentTypeDic> extends IBaseService {

    List<LoadDocumentTypeDic> getNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType);
    List<LoadDocumentTypeDic> getNotNeedDocTypes(Integer costTypeId, Integer methodTypeId, Integer polyType);
}
