package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 06.06.2016.
 */
public interface IPolyTypeService<PolyTypeDic> extends IBaseService{

    public List<PolyTypeDic> getPolyType();
}
