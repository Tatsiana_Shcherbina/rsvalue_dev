package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.Subject;

import java.util.List;

/**
 * Created by tihonovichp on 10.11.2016.
 */
public interface ISubjectService extends IBaseService<Subject> {

    List<String> getSubjectNames();

    Subject getSubjectByFullName(String surname, String firstName, String fatherName);

    Long getIdByFullName(String surname, String firstName, String fatherName);

    String getFullNameById(Long id);
}
