package org.nka.rs.service.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.nka.rs.entity.constant.commonConstant.IConstant;
import org.nka.rs.entity.dictionary.CurrencyDic;
import org.nka.rs.service.ICurrencyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by zgurskaya on 27.07.2016.
 */
@Service
@Transactional
public class CurrencyServiceImpl extends BaseServiceImpl implements ICurrencyService, IConstant {

    private DetachedCriteria query_currency_dic = DetachedCriteria.forClass(CurrencyDic.class);

    @Override
    public CurrencyDic findByCodeName(String name) {
        CurrencyDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_currency_dic);

        query_.add(Restrictions.eq("codeName", name));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (CurrencyDic) super.getUniqueResult(query_);
        return  dictionary;
    }

    @Override
    public CurrencyDic findByShortName(String shortName) {
        CurrencyDic dictionary = null;
        DetachedCriteria query_ = (DetachedCriteria) SerializationUtils.clone(query_currency_dic);

        query_.add(Restrictions.eq("codeShortName", shortName));
        query_.add(Restrictions.eq("status", RELEVANT));
        dictionary = (CurrencyDic) super.getUniqueResult(query_);
        return  dictionary;
    }

}
