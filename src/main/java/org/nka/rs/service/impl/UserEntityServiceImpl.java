package org.nka.rs.service.impl;

import org.nka.rs.dao.IUserEntityDao;
import org.nka.rs.entity.pojos.common.UserEntity;
import org.nka.rs.service.IUserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tihonovichp on 03.11.2016.
 */

@Service
@Transactional
public class UserEntityServiceImpl extends BaseServiceSessionImpl<UserEntity> implements IUserEntityService {

    @Autowired
    IUserEntityDao userEntityDao;

    @Override
    public UserEntity getUserByName(String username) {
        return userEntityDao.getUserByName(username);
    }

    @Override
    public Long getIdByName(String username) {
        return userEntityDao.getIdByName(username);
    }

    @Override
    public List<String> getLogins() {
        return userEntityDao.getLogins();
    }

    @Override
    public void logout(String username, String token) {
        userEntityDao.logout(username, token);
    }

    @Override
    public String createAndSaveToken(Long id, String username) {
        return userEntityDao.createAndSaveToken(id, username);
    }

    @Override
    public String getToken(Long id) {
        return userEntityDao.getToken(id);
    }
}
