package org.nka.rs.service.impl;

import com.itextpdf.text.DocumentException;
import org.nka.rs.dao.IMessageDao;
import org.nka.rs.dao.impl.MessageDaoImpl;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.*;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.print.PdfMessageBuilder;
import org.nka.rs.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 05.10.2016.
 */
@Service
@Transactional
public class MessageServiceImpl implements IMessageService {


    @Autowired
    IMessageDao messageDao = new MessageDaoImpl();

    @Override
    public ResponseWithData cadastrValueSearch(Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
        return messageDao.cadastrValueSearch(soato, blockNum, parcelNum, isNb, date);
    }

    @Override
    public ResponseWithData cadastrValueSearch(String cadnum, Boolean isNb, String date) {
        return messageDao.cadastrValueSearch(cadnum, isNb, date);
    }

    @Override
    public AddressMessage addressSearch(Long objectnumber, Boolean isNb, String date) {
        return messageDao.addressSearch(objectnumber, isNb, date);
    }

    @Override
    public AddressMessage mapSearch(String coord, Boolean isCadNum) {
        return messageDao.mapSearch(coord, isCadNum);
    }

    @Override
    public ResponseWithData pointValueSearch(String address, String zone, Boolean isNb, String date) {
        return messageDao.pointValueSearch(address, zone, isNb, date);
    }

    @Override
    public CadastralValueMessage communityGardenNameSearch(Long zoneId, Boolean isNb, String date) {
        return messageDao.communityGardenNameSearch(zoneId, isNb, date);
    }

    @Override
    public CadastralValueMessage landsOutLocalitySearch(Long zoneId, Boolean isNb, String date) {
        return messageDao.landsOutLocalitySearch(zoneId, isNb, date);
    }

    @Override
    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        return messageDao.farmLandsUNPSearch(objectnumber, unp, isNb, date);
    }

    @Override
    public CadastralValueMessage farmLandsNameSearch(Long objectnumber, String tenantName, Boolean isNb, String date) {
        return messageDao.farmLandsNameSearch(objectnumber, tenantName, isNb, date);
    }

    @Override
    public CadastralValueMessage farmLandsZonenNumberSearch(String zoneId, Boolean isNb, String date) {
        return messageDao.farmLandsZonenNumberSearch(zoneId, isNb, date);
    }

    @Override
    public List<String[]> getCommunityGardenList(Long objectnumber, String date) {
        return messageDao.getCommunityGardenList(objectnumber, date);
    }

    @Override
    public List<String> getTenantNameList(Long objectnumber, String date) {
        return messageDao.getTenantNameList(objectnumber, date);
    }

    @Override
    public List<String> getLandOutLocalityList(Integer funcode, Long objectnumber, String date) {
        return messageDao.getLandOutLocalityList(funcode, objectnumber, date);
    }

    @Override
    public List<Object []> getZoneNumberList(Integer funcode, Long objectnumber, String date, String zoneName) {
        return messageDao.getZoneNumberList(funcode, objectnumber, date, zoneName);
    }

    @Override
    public List<String []> getFarmZoneNumberList(Long objectnumber, String date) {
        return messageDao.getFarmZoneNumberList(objectnumber, date) ;
    }

    @Override
    public List getAddressElementList(Integer parentElementId, Long objectnumber){
        return messageDao.getAddressElementList(parentElementId, objectnumber);
    }

    @Override
    public List<String> getPeriodList() {
        List<String>  periods = messageDao.getPeriodList();
        List<String> validPeriods = new ArrayList<String>();
        for (int i = 0; i < periods.size(); i++){
            String s = periods.get(i);
            String [] parts = s.split(" ");
            String validPeriod = parts[0];
            validPeriods.add(validPeriod);
        }
        return validPeriods;
    }

    @Override
    public List<FunctionalPurposeDic> getFuncCode() {
        return messageDao.getFuncCode();
    }

    @Override
    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut) {
        return messageDao.getListAddress(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut);
    }

    @Override
    public ResponseWithData createPdf(PdfData data) {
        ResponseWithData response = new ResponseWithData();
        Object [] obj = new Object[2];
        PdfMessageBuilder builder = new PdfMessageBuilder();
        try {
            String url = builder.createPDF(data);
            if(url != null && !url.equals("")){
                Long id = messageDao.savePdfPath(url);
                if (id == null){
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Ошибка при сохранении пути документа в базу");
                } else {
                    url = url.replace("\\","/" );
                    url = url.replace("//", "/");
                    obj[1] = "file:/" + url;
                    obj[0] = id;
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    response.setData(obj);
                }
            } else {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при создании документа");
            }

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    public String searchPathById(Long id) {
        return messageDao.searchPathById(id);
    }

    @Override
    public void deletePdfById(Long id) {
        messageDao.deletePdfById(id);
    }


}
