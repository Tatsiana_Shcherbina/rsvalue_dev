package org.nka.rs.service.impl;

import org.nka.rs.dao.IUserAccountDao;
import org.nka.rs.entity.pojos.common.FilterResponse;
import org.nka.rs.entity.pojos.common.UserAccount;
import org.nka.rs.entity.pojos.common.UserData;
import org.nka.rs.service.IUserAccountService;
import org.nka.rs.util.nativedll.NativeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tihonovichp on 03.11.2016.
 */

@Service
@Transactional
public class UserAccountServiceImpl extends BaseServiceSessionImpl<UserAccount> implements IUserAccountService {

    @Autowired
    private IUserAccountDao userAccountDao;

    @Override
    public List<UserAccount> getAll(Integer startRecord, Integer recordCount) {
        return userAccountDao.getAllElements(startRecord, recordCount);
    }

    @Override
    public Serializable add(UserData userData) {
        userData.setPassword(NativeImpl.getHash(userData.getUsername() + userData.getPassword()));
        System.out.println(userData.getPassword());
        return userAccountDao.addElement(userData);
    }

    @Override
    public Long getRowCount() {
        return userAccountDao.getRowCount();
    }

    @Override
    public UserAccount getFullAccount(Long id) {
        return userAccountDao.getFullAccount(id);
    }

    @Override
    public UserAccount getAccountByName(String username) {
        return userAccountDao.getAccountByName(username);
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String position, String group, Integer startRecord, Integer recordCount) {
        return userAccountDao.getFilterElements(surname, login, org, position, group, startRecord, recordCount);
    }

    @Override
    public List<UserAccount> getFilterElements(String surname, String login, String org, String group, Integer startRecord, Integer recordCount) {
        return userAccountDao.getFilterElements(surname, login, org, group, startRecord, recordCount);
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String position, String group) {
        return userAccountDao.getFilterRowCount(surname, login, org, position, group);
    }

    @Override
    public Long getFilterRowCount(String surname, String login, String org, String group) {
        return userAccountDao.getFilterRowCount(surname, login, org, group);
    }

    @Override
    public void updateElement(UserData userData) {
        if (userData.getPassword().equals("")) {
            userAccountDao.updateElement(userData, false);
        } else {
            userData.setPassword(NativeImpl.getHash(userData.getUsername() + userData.getPassword()));
            userAccountDao.updateElement(userData, true);
        }
    }

    @Override
    public List<FilterResponse> getFilterFunction(String surname, String login, Integer org, Integer group, Integer page, Integer recordCount) {
        return userAccountDao.getFilterFunction(surname, login, org, group, page, recordCount);
    }
}
