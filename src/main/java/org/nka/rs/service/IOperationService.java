package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.Operation;

import java.sql.Date;

/**
 * Created by tihonovichp on 11.11.2016.
 */

public interface IOperationService extends IBaseService<Operation>{

    Operation getElementById(Long id);

    Long getIdByDate(Date date);
}
