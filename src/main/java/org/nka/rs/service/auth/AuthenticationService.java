package org.nka.rs.service.auth;

import org.nka.rs.dao.JDBCDao.AuthenticationDao;
import org.nka.rs.entity.pojos.security.UserDetail;
import org.nka.rs.entity.security.UserAuthorization;
import org.nka.rs.util.nativedll.NativeImpl;

/**
 * Created by Shcherbina on 26.11.2016.
 */
public class AuthenticationService {
    public UserAuthorization isAuth(String login, String pass){
        String passHash = NativeImpl.getHash(login+pass);
        System.out.println(passHash);
        AuthenticationDao dao = new AuthenticationDao();
        return dao.isAuth(login, passHash);
    }

    public UserDetail returnUser(String login, String token){
        return null;
    }
}
