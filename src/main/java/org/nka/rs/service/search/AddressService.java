package org.nka.rs.service.search;

import org.nka.rs.dao.JDBCDao.AddressDAO;
import org.nka.rs.entity.pojos.load.ATE;

import java.util.List;

/**
 * Created by tihonovichp on 06.12.2016.
 */
public class AddressService {

    private AddressDAO dao = new AddressDAO();

    public List<ATE> findAteForOtherCostType(Long parentateId) {return dao.findAteForOtherCostType(parentateId);
    }
}
