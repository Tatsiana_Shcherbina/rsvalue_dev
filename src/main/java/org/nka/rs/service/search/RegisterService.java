package org.nka.rs.service.search;

import org.nka.rs.dao.JDBCDao.RegisterDAO;
import org.nka.rs.entity.dto.DataForRegistrWork;
import org.nka.rs.entity.dto.Registr;
import org.nka.rs.entity.dto.ThematicLayer;

import java.util.List;

/**
 * Created by tihonovichp on 01.12.2016.
 */
public class RegisterService {

    private RegisterDAO dao = new RegisterDAO();

    public Object[] getRegister(DataForRegistrWork registrData) {
        return dao.getRegistr(registrData);
    }

    public Registr getLoadInf(Long regCode, Integer funcCode) {
        return dao.getLoadInf(regCode, funcCode);
    }

    public List<ThematicLayer> getLayerList(Long regCode) {
        return dao.getLayerList(regCode);
    }

    public List<Object[]> getZoneList(Long regCode, Integer funcCode) {
        return dao.getZoneList(regCode, funcCode);
    }
}
