package org.nka.rs.service.search;

import com.itextpdf.text.DocumentException;
import org.nka.rs.dao.JDBCDao.MessageDAO;
import org.nka.rs.entity.constant.commonConstant.ErrorCodeEnum;
import org.nka.rs.entity.dictionary.FunctionalPurposeDic;
import org.nka.rs.entity.dto.emessage.AddressMessage;
import org.nka.rs.entity.dto.emessage.CadastralValueMessage;
import org.nka.rs.entity.dto.emessage.PdfData;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.print.PdfMessageBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by tihonovichp on 02.12.2016.
 */
public class MessageService {

    private MessageDAO dao = new MessageDAO();

    public AddressMessage addressSearch(Long objectnumber, Boolean isNb, String date) {
        return dao.addressSearch(objectnumber, isNb, date);
    }

    public ResponseWithData cadastrValueSearch(Long soato, Integer blockNum, Integer parcelNum, Boolean isNb, String date) {
        return dao.cadastrValueSearch(soato, blockNum, parcelNum, isNb, date);
    }

    public ResponseWithData cadastrValueSearch(String cadNum, Boolean isNb, String date) {
        return dao.cadastrValueSearch(cadNum, isNb, date);
    }

    public ResponseWithData pointValueSearch(String address, String zone, Boolean isNb, String date) {
        return dao.pointValueSearch(address, zone, isNb, date);
    }

    public CadastralValueMessage communityGardenNameSearch(Long zoneId, Boolean isNb, String date) {
        return dao.communityGardenNameSearch(zoneId, isNb, date);
    }

    public CadastralValueMessage landsOutLocalitySearch(Long zoneId, Boolean isNb, String date) {
        return dao.landsOutLocalitySearch(zoneId, isNb, date);
    }

    public CadastralValueMessage farmLandsUNPSearch(Long objectnumber, Long unp, Boolean isNb, String date) {
        return dao.farmLandsUNPSearch(objectnumber, unp, isNb, date);
    }

    public CadastralValueMessage farmLandsNameSearch(Long objectnumber, String tenantName, Boolean isNb, String date) {
        return dao.farmLandsNameSearch(objectnumber, tenantName, isNb, date);
    }

    public CadastralValueMessage farmLandsZonenNumberSearch(String zoneId, Boolean isNb, String date) {
        return dao.farmLandsZonenNumberSearch(zoneId, isNb, date);
    }

    public List<String[]> getCommunityGardenList(Long objectnumber, String date) {
        return dao.getCommunityGardenList(objectnumber, date);
    }

    public List<String> getTenantNameList(Long objectnumber, String date) {
        return dao.getTenantNameList(objectnumber, date);
    }

    public List<Object[]> getZoneNumberList(Integer funcode, Long objectnumber, String date, String zoneName) {
        return dao.getZoneNumberList(funcode, objectnumber, date, zoneName);
    }

    public List<String[]> getFarmZoneNumberList(Long objectnumber, String date) {
        return dao.getFarmZoneNumberList(objectnumber, date);
    }

    public List<Object[]> getAddressElementList(Integer parentElementId, Long objectnumber) {
        return dao.getAddressElementList(parentElementId, objectnumber);
    }

    public List<String> getLandOutLocalityList(Integer funcode, Long objectnumber, String date) {
        return dao.getLandOutLocalityList(funcode, objectnumber, date);
    }

    public List<String> getPeriodList() {
        return dao.getPeriodList();
    }

    public List getListAddress(Long region, Long area, Long ss, Long snp, Long bjd, Long road, Long km, Long street, Integer typeIn, Integer typeOut) {
        return dao.getListAddress(region, area, ss, snp, bjd, road, km, street, typeIn, typeOut);
    }

    public List<FunctionalPurposeDic> getFuncCode() {
        return dao.getFuncCode();
    }

    public Long savePdfPath(String url) {
        return dao.savePdfPath(url);
    }

    public String searchPathById(Long id) {
        return dao.searchPathById(id);
    }

    public ResponseWithData createPdf(PdfData data) {
        ResponseWithData response = new ResponseWithData();
        Object [] obj = new Object[2];
        PdfMessageBuilder builder = new PdfMessageBuilder();
        try {
            String url = builder.createPDF(data);
            if(url != null && !url.equals("")){
                Long id = dao.savePdfPath(url);
                if (id == null){
                    response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    response.setMessage("Ошибка при сохранении пути документа в базу");
                } else {
                    url = url.replace("\\","/" );
                    url = url.replace("//", "/");
                    obj[1] = "file:/" + url;
                    obj[0] = id;
                    response.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    response.setData(obj);
                }
            } else {
                response.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                response.setMessage("Ошибка при создании документа");
            }

        } catch (DocumentException e) {
            System.out.println(e.getMessage());;
        } catch (IOException e) {
            System.out.println(e.getMessage());;
        }
        return response;
    }

    public Response deletePdfById(Long id) {
        Response res = new Response();
        res.setCode(ErrorCodeEnum.SUCCESS.getValue());
        String path = "";
        path = dao.searchPathById(id);
        if (path != null && !path.equals("")) {
            File serverFile = new File(path);
            if (serverFile.exists()) {
                Boolean isDelete;
                isDelete = serverFile.delete();
                if (!isDelete) {
                    res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                    res.setMessage("Не могу удалить файл по пути " + path + ". Обратитесь к администратору");
                } else {
                    res.setCode(ErrorCodeEnum.SUCCESS.getValue());
                    res.setMessage("Файл удален");
                    dao.deletePdfById(id);
                }
            } else {
                res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
                res.setMessage("По указанному пути " + path + " файла не существует. Обратитесь к администратору");
            }
        } else {
            res.setCode(ErrorCodeEnum.UNSUCCESS.getValue());
            res.setMessage("Ошибка при поиске пути к файлу. Обратитесь к администратору");
        }
        return res;
    }

}
