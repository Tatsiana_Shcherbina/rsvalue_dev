package org.nka.rs.service;

import org.nka.rs.entity.responseVar.ResponseWithData;
import org.nka.rs.entity.dto.AdditionalLayer;
import org.nka.rs.entity.dto.LayerType;

import java.util.List;

/**
 * Created by zgurskaya on 09.06.2016.
 */
public interface ILayerTypeService extends IBaseService{

    //получение списка только валидных слоев
    List<LayerType> getValideLayers(List<String> layerNames);
    //получение полного списка - если нет аналога - данные null
    List<LayerType> getLayersData(List<String> layerNames);
    //если нет аналогов в классификаторе - выводится сообщение о невалидности данных
    ResponseWithData getValideLayerName(List<String> layerNames);
    //проверка для доступности транспорта и грунта
    ResponseWithData getValideLayerValue(AdditionalLayer[] data);

    }
