package org.nka.rs.service;

import java.util.List;

/**
 * Created by zgurskaya on 01.06.2016.
 */
public interface IAddressService<ATE> extends IBaseService {

    public List<ATE> findATEbyName(String ateName);

    public List<ATE> expandATE(String Tree_ids);

    public List<ATE> findATEbyParentId(Long parentateId);

    public List<ATE> findATEbyParentLike(Long parentateId, String ateName);

    public List<ATE> findATEbyCategory(Integer categoryId);

    public String fillParentAte(ATE ate);

    List<ATE> findAteForCityCostType(Long parentateId);

    List<ATE> findAteForOtherCostType(Long parentateId);

    List<Long> findAteIdForParcelOutNP(Long parentateId);

    List<Long> findAteIdForSNP(Long parentateId);

}
