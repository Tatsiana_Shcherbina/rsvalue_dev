package org.nka.rs.service;

/*
Created by zgurskaya on 06.06.2016.
*/


import org.json.JSONObject;
import org.nka.rs.entity.dictionary.LoadDocumentTypeDic;
import org.nka.rs.entity.dto.*;
import org.nka.rs.entity.responseVar.Response;
import org.nka.rs.entity.responseVar.SaveDocResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IRegistrService extends IBaseService {

    /*groupParam - группирующие показатели, списком из номеров столбцов, sorting - параметры сортировки,
    page - номер страницы, на которую переходим, amount - количество записей на странице
    dateFrom - с какой даты,  dateTo - по какую дату*/

    //получить список данных для журнала загрузок
    Object[] getRegistr(DataForRegistrWork param);
    //получить список данных для настраиваемого журнала загрузок
    /*Object[] getManagedRegistr(DataForRegistrWork param);
    Object[] getAllDataRecords(DataForRegistrWork param);*/
    //получить запись об объекте загрузки по идентификатору
    Registr getUniqueRecord(Long regNum);
    //получить запись об объекте загрузки по идентификатору
    Registr getUniqueRecord(Long regNum, String funCode);
    //получить список документов для данной загрузки
    List<DocumentData> getDocumentList(Long regNum);
    //получить список тематических слоев для данной загрузки
    List<ThematicLayer> getLayerList(Long regNum);
    //получить список зон для данной загрузки
    Object[] getZoneList(Long regNum, Integer sortingParam, String predicate, Integer page);
    //получить настраиваемый список зон для данной загрузки
    Object[] getManagedZoneList(Long regNum, Integer [] columnNums, Integer sortingParam, String predicate, Integer page);
    //получить список АТЕ по части наименования
    List searchByATE(String partName);
    //скачать документ
    MultipartFile downloadDoc(Long docId);
    String downloadFile(Long docId);
    //получение списка необязательных документов для данной загрузки
    List<LoadDocumentTypeDic> getDocTypeList(Long regNum);
    //редактирование документов (в том числе и тематических слоев) - это сохранение/отмена внесенных изменений
    Response saveEditDoc(EditDocumentData data);
    //запись нового документа при редактировании. до работы saveEditDoc - неактивен.
    SaveDocResponse editFile(MultipartFile file, Integer docTypeId, Long userId, Long regNum);
    //запись нового тематического слоя при редактировании. до работы saveEditDoc - неактивен.
    Response editLayer(JSONObject geojson, Long reg_num, String layerName, Long userId);

}
