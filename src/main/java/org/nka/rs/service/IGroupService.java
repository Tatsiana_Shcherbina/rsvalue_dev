package org.nka.rs.service;

import org.nka.rs.entity.pojos.common.Group;

import java.util.List;

/**
 * Created by tihonovichp on 08.11.2016.
 */

public interface IGroupService extends IBaseService<Group>{

    List<String> getGroupsNames();

    Integer getIdByName(String name);
}
